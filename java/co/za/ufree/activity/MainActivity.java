package co.za.ufree.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import co.za.ufree.R;
import co.za.ufree.fragment.ChatroomFragment;
import co.za.ufree.fragment.DashBoardFragment;
import co.za.ufree.fragment.ReplyFragment;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/04/24.
 */

public class MainActivity extends Application {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FacebookSdk.sdkInitialize(getApplicationContext());

        Log.i(Constants.TAG, String.format("%s : %s", "Starting Fragment", getClass().toString()));
        Log.i(Constants.TAG, String.format("Device Id : %s", getDeviceId()));

        saveSettings(getApplication().getString(R.string.setting_device_id), getDeviceId());

        //Check if user already logged in
        if (getSettings(getApplication().getString(R.string.setting_skip_login)) == null) {
            changeFragment(Constants.LOGIN_FRAGMENT);
        } else {
            changeFragment(Constants.DASHBOARD_FRAGMENT);
        }

    }


    //Log user out
    private void logoutUser() {
        Application.deleteSettings(getApplication().getString(R.string.setting_skip_login));
        Application.deleteSettings(getApplication().getString(R.string.setting_device_id));
        LoginManager.getInstance().logOut();

        changeFragment(Constants.LOGIN_FRAGMENT);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
