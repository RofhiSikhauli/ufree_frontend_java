package co.za.ufree.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import java.io.PrintWriter;
import java.io.StringWriter;

import co.za.ufree.R;
import co.za.ufree.fragment.ChatroomFragment;
import co.za.ufree.fragment.ComposePostFragment;
import co.za.ufree.fragment.DashBoardFragment;
import co.za.ufree.fragment.LoginFragment;
import co.za.ufree.fragment.ReadMoreFragment;
import co.za.ufree.fragment.ReplyFragment;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/05/02
 */
public class Application extends AppCompatActivity {

    public static SharedPreferences sharedPreferences;

    // If app crashes, send email to the below emails
    Thread.UncaughtExceptionHandler handleAppCrash = (thread, ex) -> {
        // Convert stacktrace to String
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "uFree has stopped working");

        intent.putExtra(Intent.EXTRA_EMAIL, getApplication().getResources().getStringArray(R.array.support_recipients));
        intent.putExtra(Intent.EXTRA_TEXT, sw.toString());
        Intent mailer = Intent.createChooser(intent, null);

        // open default email application to send stacktrace
        startActivity(mailer);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Thread.setDefaultUncaughtExceptionHandler(handleAppCrash);
        sharedPreferences = getApplication().getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
    }

    public static void saveSettings(String settingName, String settingValue) {
        sharedPreferences.edit().putString(settingName, settingValue).apply();
    }

    public static String getSettings(String settingName) {
        return sharedPreferences.getString(settingName, null);
    }

    public static void deleteSettings(String settingName) {
        sharedPreferences.edit().remove(settingName).apply();
    }

    public void changeFragment(int appFragmentId) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (appFragmentId) {
            case Constants.LOGIN_FRAGMENT:
                fragmentTransaction.replace(R.id.fragmentHolder, new LoginFragment().setParentFragment(this));
                break;
            case Constants.DASHBOARD_FRAGMENT:
                fragmentTransaction.replace(R.id.fragmentHolder, new DashBoardFragment().setParentFragment(this));
                break;
        }
        fragmentTransaction.commit();
    }

    public void changeFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragmentHolder, fragment);
        fragmentTransaction.addToBackStack(getResources().getString(R.string.app_name));
        fragmentTransaction.commit();
    }

    public String getDeviceId() {
        return Settings.Secure.getString(getApplication().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public String getApplicationVersion() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}

