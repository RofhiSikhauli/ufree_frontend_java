package co.za.ufree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by uFree_Dev_Team on 2015/04/28.
 */
public class BaseResponse {

    @SerializedName(value = "code")
    private String responseCode;

    @SerializedName(value = "status")
    private String responseMessage;

    @SerializedName(value = "user_id")
    public String userId;

    public BaseResponse() {

    }

    public BaseResponse(String responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
