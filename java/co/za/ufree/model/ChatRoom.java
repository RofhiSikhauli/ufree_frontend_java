package co.za.ufree.model;

/**
 * Created by Simon on 2015/08/16.
 */
public class ChatRoom {
    private String id;
    private String title;
    private String description;
    private int radius;

    public ChatRoom() {
    }

    public ChatRoom(String id, String title, String description, int radius) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.radius = radius;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "ChatRoom{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", radius='" + radius + '\'' +
                '}';
    }
}
