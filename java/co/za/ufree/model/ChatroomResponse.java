package co.za.ufree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2015/07/09.
 */


public class ChatroomResponse {


    @SerializedName(value = "chatroom_id")
    public int chatroom_id;
    @SerializedName(value = "title")
    public String title;
    @SerializedName(value = "description")
    public String description;
    @SerializedName(value = "time")
    public String time;
    @SerializedName(value = "updates")
    public String updates;
    @SerializedName(value = "radius")
    public int radius;
    @SerializedName(value = "username")
    public String username;
    @SerializedName(value = "text_message")
    public String text_message;

    public ChatroomResponse(){
    }

    public ChatroomResponse(String username, String text_message, String time) {
        this.username = username;
        this.text_message = text_message;
        this.time = time;
    }


    //Get chatroom title
    public String getTitle() {
        return title;
    }

    //Set chatroom title
    public void setTile(String title) {
        this.title = title;
    }

    //Get chatroom description
    public String getDescription() {
        return description;
    }

    //Set chatroom description
    public void setDescription(String description) {
        this.title = description;
    }


    //Get time
    public String getTime() {
        return time;
    }

    //Set time
    public void setTime(String time) {
        this.time = time;
    }


    //Get updates
    public String getUpdates() {
        return updates;
    }

    //Set updates
    public void setUpdates(String updates) {
        this.updates = updates;
    }

    //Get chatroom radius
    public int getRadius() {
        return radius;
    }

    //Set chatroom radius
    public void setRadius(int radius) {
        this.radius = radius;
    }


    //Get chatroom id
    public int getChatroomId() {
        return chatroom_id;
    }

    //Set chatroom id
    public void setChatroomId(int chatroom_id) {
        this.chatroom_id = chatroom_id;
    }

    //Get chatroom username
    public String getUsername() {
        return username;
    }

    //Set chatroom username
    public void setUsername(String username) {
        this.username = username;
    }


    //Get chatroom message
    public String getTextMessage() {
        return text_message;
    }

    //Set chatroom message
    public void setTextMessage(String text_message) {
        this.text_message = text_message;
    }




}
