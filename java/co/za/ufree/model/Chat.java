package co.za.ufree.model;

/**
 * Created by Simon on 2015/08/16.
 */
public class Chat {
    private String username;
    private String text_message;
    private String time;

    public Chat(){}

    public Chat(String username, String text_message, String time) {
        this.username = username;
        this.text_message = text_message;
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText_message() {
        return text_message;
    }

    public void setText_message(String text_message) {
        this.text_message = text_message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "username='" + username + '\'' +
                ", text_message='" + text_message + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
