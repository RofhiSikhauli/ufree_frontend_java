package co.za.ufree.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import co.za.ufree.fragment.PostsFragment;

/**
 * Created by uFree_Dev_Team on 2015/05/02.
 */
public class PostsResponse extends BaseResponse {

    @SerializedName(value = "post_id")
    private String id;
    @SerializedName(value = "Post")
    private String post;
    @SerializedName(value = "coordinates")
    private String coordinates;
    @SerializedName(value = "seen_by")
    private String seenBy;
    @SerializedName(value = "likes_count")
    private String likesCount;
    @SerializedName(value = "dislike_count")
    private String dislikeCount;
    @SerializedName(value = "reply_count")
    private String replyCount;
    @SerializedName(value = "timestamp")
    private String timestamp;
    private ArrayList<PostReply> replies;
    private String posted;
    private Flag flags;

    public PostsResponse(){}

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getSeenBy() {
        return seenBy;
    }

    public void setSeenBy(String seenBy) {
        this.seenBy = seenBy;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public String getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(String dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public String getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(String replyCount) {
        this.replyCount = replyCount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Flag getFlags() {
        return flags;
    }

    public ArrayList<PostReply> getReplies() {
        return replies;
    }

    public void setReplies(ArrayList<PostReply> replies) {
        this.replies = replies;
    }

    public void setFlags(Flag flags) {
        this.flags = flags;
    }

    public static class Flag{
        private String like;
        private String dislike;

        public String getLike() {
            return like;
        }

        public void setLike(String like) {
            this.like = like;
        }

        public String getDislike() {
            return dislike;
        }

        public void setDislike(String dislike) {
            this.dislike = dislike;
        }
    }

    public static class PostReply{
        private String reply, posted;

        public String getReply() {
            return reply;
        }

        public void setReply(String reply) {
            this.reply = reply;
        }

        public String getReplyTime() {
            return posted;
        }

        public void setReplyTime(String posted) {
            this.posted = posted;
        }


    }




}
