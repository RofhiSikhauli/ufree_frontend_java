package co.za.ufree.listener;

import android.content.Context;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.adapter.PostsRecycleViewAdapter;
import co.za.ufree.fragment.DashBoardFragment;
import co.za.ufree.model.BaseResponse;
import co.za.ufree.model.PostsResponse;
import co.za.ufree.task.LoadPostsTask;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/05/21.
 */
public class ScrollPostsListener extends RecyclerView.OnScrollListener {

    private static final int HIDE_THRESHOLD = 20;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;
    private boolean loading = false;
    protected RecyclerView posts;
    private LinearLayoutManager mLayoutManager;
//    private Application application;
    private ArrayList<PostsResponse> postsResponses;
    private PostsRecycleViewAdapter postsAdapter;
    private Context context;
    private String postsType;
    private String deviceID;

    public ScrollPostsListener(String postsType, RecyclerView posts, LinearLayoutManager mLayoutManager, ArrayList<PostsResponse> postsResponses, PostsRecycleViewAdapter postsAdapter, Context mContext, String deviceID) {
        this.posts = posts;
        this.mLayoutManager = mLayoutManager;
        this.postsResponses = postsResponses;
        this.postsAdapter = postsAdapter;
        this.context = mContext;
        this.postsType = postsType;
        this.deviceID = deviceID;
    }


    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = posts.getChildCount();
        int totalItemCount = mLayoutManager.getItemCount();
        int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
        int visibleThreshold = 5;

        String start = Application.getSettings(context.getString(R.string.param_sel_limit) + postsType);

        Log.i("Start ", start);
        if (loading) {
            if (totalItemCount > Integer.parseInt(start)) {
                loading = false;
            }
        }

        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            loading = true;
            new LoadPostsTask(context, response -> {
                try {
                    ArrayList<PostsResponse> responses = Constants.GSON.fromJson((String) response,
                            new TypeToken<ArrayList<PostsResponse>>() {
                            }.getType());
                    if (responses.size() > 0) {
                        postsResponses.addAll(responses);
                        postsAdapter.refreshData(postsResponses);
                        loading = false;
                    }
                } catch (JsonSyntaxException ex) {
                    // TODO Can't load more post DO something here
                    BaseResponse baseResponse = Constants.GSON.fromJson((String) response, BaseResponse.class);
                }
            }).execute(Application.getSettings(context.getString(R.string.param_user_id)),
                    deviceID, start, String.valueOf(Integer.parseInt(start) + 5),
                    String.valueOf(postsType));

            Application.saveSettings(context.getString(R.string.param_sel_limit) + postsType, String.valueOf(Integer.parseInt(start) + 5));
        }
    }


}
