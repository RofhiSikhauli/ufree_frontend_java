package co.za.ufree.util;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import co.za.ufree.R;

/**
 * Created by uFree_Dev_Team on 2015/04/26.
 */
public class Constants {

    // App logging tag
    public static final String TAG = "CYBER-DRAGON";

    public static final boolean DEBUG = true;

    // Add more fragments here
    public static final int LOGIN_FRAGMENT = 1;
    public static final int DASHBOARD_FRAGMENT = 2;
    public static final int ADD_REPLY_FRAGMENT = 3;
    public static final int READ_MORE_FRAGMENT = 4;
    public static final int COMPOSE_POST_FRAGMENT = 5;
    public static final int CHATROOM_FRAGMENT = 6;
    public static final int CHATROOM_CONVERSATION_FRAGMENT = 7;

    public static final Gson GSON = new Gson();
    public static final HttpUtils HTTP_UTILS = new HttpUtils();

    public static Toast showToastMessage(Context context, String toastMessage) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.updates_lists, null);

        TextView text = (TextView) layout.findViewById(R.id.textViewToast);
        if (toastMessage != null)
            text.setText(toastMessage);
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.CENTER_VERTICAL | Gravity.BOTTOM, 0, 0);
        toast.setView(layout);
        return toast;
    }
}
