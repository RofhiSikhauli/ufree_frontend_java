package co.za.ufree.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import co.za.ufree.model.BaseResponse;

/**
 * Created by uFree_Dev_Team on 04 May 2015.
 */
public class HttpUtils {

    HttpURLConnection urlConnection;
    String CHAR_ENCODING = "UTF-8";
    String CONTENT_TYPE_X_WWW_FORM = "application/x-www-form-urlencoded;charset=UTF-8";

    public String sendHttpPostRequestWithParams(String url, Map<String, Object> httpPostParams) {
        try {
            byte[] postDataBytes = setParameters(httpPostParams).toString().getBytes(CHAR_ENCODING);

            URL post = new URL(url);
            urlConnection = (HttpURLConnection) post.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Accept-Charset", CHAR_ENCODING);
            urlConnection.setRequestProperty("Content-Type", CONTENT_TYPE_X_WWW_FORM);
            urlConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            urlConnection.getOutputStream().write(postDataBytes);

            return convertStreamToString(urlConnection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return Constants.GSON.toJson(new BaseResponse(ResponseCodes.SERVICE_NOT_FOUND.getCode(),
                    ResponseCodes.SERVICE_NOT_FOUND.getMessage()));
        }
    }

    public String sendHttpGetRequestWithParams(String stringUrl, Map<String, Object> params) {
        try {
            URL url = new URL(stringUrl + "?" + setParameters(params));
            urlConnection = (HttpURLConnection) url.openConnection();

            return convertStreamToString(urlConnection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return Constants.GSON.toJson(new BaseResponse(ResponseCodes.SERVICE_NOT_FOUND.getCode(),
                    ResponseCodes.SERVICE_NOT_FOUND.getMessage()));
        }
    }

    public String setParameters(Map<String, Object> params) {
        try {
            StringBuilder parameters = new StringBuilder();
            for (Map.Entry<String, Object> param : params.entrySet()) {
                if (parameters.length() != 0) {
                    parameters.append('&');
                }
                parameters.append(URLEncoder.encode(param.getKey(), CHAR_ENCODING));
                parameters.append('=');
                parameters.append(URLEncoder.encode(String.valueOf(param.getValue()), CHAR_ENCODING));
            }
            return parameters.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String convertStreamToString(InputStream inputStream) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String temp, response = "";
            while ((temp = reader.readLine()) != null) {
                response += temp;
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
