package co.za.ufree.util;

/**
 * Created by uFree_Dev_Team on 2015/05/12.
 */
public enum ResponseCodes {

    SUCCESS("200", "Success"),
    FAIL("401", "Fail"),
    NOT_FOUND("403","Post not found"),
    SERVICE_URL_ERROR("556", "Bad URI syntax"),
    SERVICE_NOT_FOUND("555", "Server not found");

    private String code;
    private String message;

    ResponseCodes(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
