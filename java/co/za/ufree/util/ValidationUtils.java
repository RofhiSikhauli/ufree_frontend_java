package co.za.ufree.util;

import android.util.Patterns;
import android.widget.EditText;
import java.util.regex.Pattern;

/**
 * Created by root on 2015/05/06.
 */
public class ValidationUtils {

    public static boolean isValidateUserInput(EditText editText, String error) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(error);
            return false;
        }
        return true;
    }

    public static boolean isValidEmail(EditText editText, String error) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if(!pattern.matcher(editText.getText().toString()).matches()){
            editText.setError(error);
            return false;
        }
        return true;
    }

}
