package co.za.ufree.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import co.za.ufree.R;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/05/02.
 */
public class LoadPostsTask extends AsyncTask<String, Void, String> {
    protected Context context;
    protected CallBackTask callBackTask;

    public LoadPostsTask(Context context, CallBackTask<String> callBackTask) {
        this.context = context;
        this.callBackTask = callBackTask;
    }

    @Override
    protected String doInBackground(String... params) {
        Log.i(Constants.TAG, String.format("User ID : %s, App ID : %s, Sel Start : %s, Sel Limit : %s", params[0], params[1], params[2], params[3]));
        Map<String, Object> httpParams = new HashMap<>();
        httpParams.put(context.getString(R.string.param_user_id), params[0]);
        httpParams.put(context.getString(R.string.param_app_id), params[1]);
        httpParams.put(context.getString(R.string.param_sel_start), params[2]);
        httpParams.put(context.getString(R.string.param_sel_limit), params[3]);
        httpParams.put("post_type", params[4]);

        String response = Constants.HTTP_UTILS.sendHttpGetRequestWithParams(
                context.getString(R.string.service_base) + context.getString(R.string.service_load_posts), httpParams);
        Log.i(Constants.TAG, String.format("%s : %s", "Response Message", response));
        return response;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        callBackTask.runCallback(response);
    }
}
