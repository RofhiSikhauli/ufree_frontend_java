package co.za.ufree.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import co.za.ufree.R;
import co.za.ufree.model.BaseResponse;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/04/28.
 */
public class LoginTask extends AsyncTask<String, Void, BaseResponse> {

    private CallBackTask callBackTask;
    private Gson gson;
    private Context context;

    public LoginTask(CallBackTask<BaseResponse> callBackTask, Context context) {
        this.callBackTask = callBackTask;
        this.gson = new Gson();
        this.context = context;
    }

    @Override
    protected BaseResponse doInBackground(String... params) {
        String postUrl;
        Map<String, Object> postParams = new HashMap<>();
        postParams.put(context.getString(R.string.param_email), params[0]);
        postParams.put(context.getString(R.string.param_password), params[1]);

        if (!params[2].equals(context.getString(R.string.service_register))) {
            postParams.put(context.getString(R.string.param_app_version), params[2]);
            postParams.put(context.getString(R.string.param_app_id), params[3]);
            postUrl = String.format("%s%s", context.getString(R.string.service_base), context.getString(R.string.service_login));
        } else {
            postUrl = String.format("%s%s", context.getString(R.string.service_base), context.getString(R.string.service_register));
        }

        String response = Constants.HTTP_UTILS.sendHttpPostRequestWithParams(postUrl, postParams);
        Log.i(Constants.TAG, String.format("%s : %s", "Response Message", response));

        return gson.fromJson(response, BaseResponse.class);
    }


    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        super.onPostExecute(baseResponse);
        callBackTask.runCallback(baseResponse);
    }

}
