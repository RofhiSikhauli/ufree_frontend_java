package co.za.ufree.task;

/**
 * Created by uFree_Dev_Team on 2015/04/28.
 */
public interface CallBackTask<T> {

    void runCallback(T response);

}