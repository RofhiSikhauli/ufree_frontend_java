package co.za.ufree.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import co.za.ufree.R;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/06/14.
 */
public class LoadPostRepliesTask extends AsyncTask<String, Void, String> {

    private CallBackTask<String> repliesCallback;
    private Context context;

    public LoadPostRepliesTask(Context context, CallBackTask<String> repliesCallback) {
        this.context = context;
        this.repliesCallback = repliesCallback;
    }

    @Override
    protected String doInBackground(String... params) {
        Log.i(Constants.TAG,"Loading replies params : "+ Arrays.toString(params));
        Map<String, Object> httpParams = new HashMap<>();
        httpParams.put(context.getString(R.string.param_user_id), params[1]);
        httpParams.put(context.getString(R.string.param_app_id), params[0]);
        httpParams.put(context.getString(R.string.param_post_id), params[2]);

        String response = Constants.HTTP_UTILS.sendHttpPostRequestWithParams(context.getString(R.string.service_base) + context.getString(R.string.service_load_replies), httpParams);

        Log.i(Constants.TAG, String.format("DONE Loading replies : %s : %s", "Response Message", response));
        return response;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        repliesCallback.runCallback(response);
    }
}
