package co.za.ufree.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.adapter.PageAdapter;
import co.za.ufree.adapter.PostsRecycleViewAdapter;
import co.za.ufree.common.view.SlidingTabLayout;
import co.za.ufree.common.view.SwipeRefreshLayoutBottom;
import co.za.ufree.listener.ScrollPostsListener;
import co.za.ufree.model.BaseResponse;
import co.za.ufree.model.PostsResponse;
import co.za.ufree.task.LoadPostsTask;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/04/24.
 */
public class DashBoardFragment extends BaseFragment<Application> {

    public Toolbar toolBarDashboard;
    private Menu menu;
    protected FloatingActionButton actionButton;
    private SlidingTabLayout tabs;
    private PageAdapter mPageAdapter;
    protected String[] mTabArray;
    public Application application;
    private ViewPager pager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(Constants.TAG, String.format("%s : %s", "Starting Main Fragment", getClass().toString()));
        Log.i(Constants.TAG, String.format("User ID : %s", Application.getSettings(getActivity().getString(R.string.param_user_id))));
        Application.saveSettings(getActivity().getString(R.string.setting_skip_login), "true");
        application = new Application();

        View mainView = inflater.inflate(R.layout.fragment_dashboard, container, false);

        toolBarDashboard = (Toolbar) mainView.findViewById(R.id.toolBarDashboard);
        getBaseParentFragment().setSupportActionBar(toolBarDashboard);

        toolBarDashboard.setTitle("Posts");
        toolBarDashboard.setLogo(R.mipmap.ic_launcher);


        actionButton = (FloatingActionButton) mainView.findViewById(R.id.float_action_button);
        actionButton.setOnClickListener(event -> {
            ComposePostFragment composePostFragment = new ComposePostFragment();
            composePostFragment.setParentFragment(this);
            getBaseParentFragment().changeFragment(composePostFragment);
        });

        getBaseParentFragment().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mTabArray = getResources().getStringArray(R.array.tabs);

        initialisePaging(mainView); //Init paging

        //Page change listener -> which page is user viewing
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                //Hide/Show FAB Button
                if(position == 0) {
                    actionButton.setVisibility(View.VISIBLE);

                }else{
                    actionButton.setVisibility(View.GONE);

                }

            }


            @Override
            public void onPageScrollStateChanged(int state) {}

        });





        return mainView;
    }


    //Init paging
    private void initialisePaging(View v) {

        this.mPageAdapter  = new PageAdapter(super.getFragmentManager(), getActivity(), pager);

        pager = (ViewPager) v.findViewById(R.id.viewpager);
        pager.setAdapter(this.mPageAdapter);

        tabs = (SlidingTabLayout) v.findViewById(R.id.tabsHost);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer((int position) ->  getResources().getColor(R.color.white));

        tabs.setCustomTabView(R.layout.tabs_layout, 0);
        tabs.setViewPager(pager);


    }


    public void hideKeyboard() {
        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    public Toolbar getDashboardToolbar(){
        return this.toolBarDashboard;
    }




}
