package co.za.ufree.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import co.za.ufree.R;
import co.za.ufree.adapter.ChatroomAdapter;
import co.za.ufree.adapter.UpdatesAdapter;
import co.za.ufree.model.ChatroomResponse;
import co.za.ufree.util.Constants;

/**
 * Created by rofhiwa.sikhauli on 8/4/2015.
 */
public class UpdateFragment extends BaseFragment<DashBoardFragment> {

    public ArrayList updates;
    public ArrayList<ChatroomResponse> valueData;
    public RecyclerView mRecyclerView;
    public RecyclerView.Adapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;
    public LinearLayout noUpdatesFound;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.updates_fragment, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        noUpdatesFound = (LinearLayout) view.findViewById(R.id.noUpdatesFound);

        updates = getUpdates(); //Get chatrooms

        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(updates.toString()).getAsJsonArray();

        valueData = new ArrayList<ChatroomResponse>();

        for(JsonElement obj : jArray) {
            ChatroomResponse chatroomList = Constants.GSON.fromJson(obj , ChatroomResponse.class);
            valueData.add(chatroomList);
        }

        mRecyclerView.setHasFixedSize(true);

        // Linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        FragmentManager fragmentManager = getFragmentManager();

        //Call adapter
        mAdapter = new UpdatesAdapter(valueData, getActivity(),  fragmentManager, noUpdatesFound);
        mRecyclerView.setAdapter(mAdapter);

        return view;

    }


    private ArrayList getUpdates(){

        ArrayList jsonArr = new ArrayList();

//        String jsonData = "{'id' : '1','time': '12 minutes ago', 'updates': 'You got a mesage from blah blah blah' }";
//        jsonArr.add(jsonData);

        return  jsonArr;

    }


}
