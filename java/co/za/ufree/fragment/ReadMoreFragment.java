package co.za.ufree.fragment;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.model.PostsResponse;
import co.za.ufree.task.LikePostTask;
import co.za.ufree.util.Constants;

/**
 * Created by root on 2015/06/17.
 */



public class ReadMoreFragment extends BaseFragment<DashBoardFragment> {

    private EditText editTextAddPost;
    private Toolbar readMoreToolBar;
    private Menu menu;
    private String postsType;
    private TextView textViewLike, textViewDislike, textViewReply;
    private PostsResponse post;
    private Toolbar toolBarDashboard;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().supportInvalidateOptionsMenu();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_read_more, null);
        post = Constants.GSON.fromJson(getArguments().getString(getActivity().getResources().getString(R.string.param_post_id)), PostsResponse.class);
        setStatus(view, post);

        textViewLike = (TextView) view.findViewById(R.id.textViewLike);
        textViewDislike = (TextView) view.findViewById(R.id.textViewDislike);

        textViewLike.setOnClickListener(click -> new LikePostTask(getActivity(), response -> {
            if (response != null) {
                setStatus(view, ((ArrayList<PostsResponse>) Constants.GSON.fromJson((String) response, new TypeToken<ArrayList<PostsResponse>>() {
                }.getType())).get(0));
            }
        }).execute(Application.getSettings(getActivity().getString(R.string.param_user_id)), Application.getSettings(getActivity().getString(R.string.setting_device_id)), post.getId(), getActivity().getResources().getString(R.string.val_on)));

        textViewDislike.setOnClickListener(click -> new LikePostTask(getActivity(), response -> {
            if (response != null) {
                setStatus(view, ((ArrayList<PostsResponse>) Constants.GSON.fromJson((String) response, new TypeToken<ArrayList<PostsResponse>>() {
                }.getType())).get(0));
            }
        }).execute(Application.getSettings(getActivity().getString(R.string.param_user_id)), Application.getSettings(getActivity().getString(R.string.setting_device_id)), post.getId(), getActivity().getResources().getString(R.string.val_off)));

        // Set up reply text view and click listener
        textViewReply = (TextView) view.findViewById(R.id.textViewReply);
        textViewReply.setOnClickListener(event -> showReplies(post));

        readMoreToolBar = (Toolbar) view.findViewById(R.id.readMoreToolBar);

        //Add toolbar to this fragment
        ((AppCompatActivity) getActivity()).setSupportActionBar(readMoreToolBar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Post");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_light);

        return view;
    }

    private void setStatus(View view, PostsResponse post) {
        int totalUpVotesCount = Integer.parseInt(post.getLikesCount());
        int totalDownVotesCount = Integer.parseInt(post.getDislikeCount());
        int totalReplyCount = Integer.parseInt(post.getReplyCount());
        String postStats = "";

        //Get upvotes plural and singular ex (1 vote, 2 votes)
        if (totalUpVotesCount == 1) {
            postStats += totalUpVotesCount + " upvote  ";
        } else if (totalUpVotesCount > 1) {
            postStats += totalUpVotesCount + " upvotes  ";
        }

        //Get downvotes plural and singular ex (1 vote, 2 votes)
        if (totalDownVotesCount == 1) {
            postStats += totalDownVotesCount + " downvote  ";
        } else if (totalDownVotesCount > 1) {
            postStats += totalDownVotesCount + " downvotes  ";
        }

        //Get votes plural and singular like 1 reply, 2 replies
        if (totalReplyCount == 1) {
            postStats += totalReplyCount + " reply";
        } else if (totalReplyCount > 1) {
            postStats += totalReplyCount + " replies";
        }

        ((TextView) view.findViewById(R.id.viewPostTime)).setText(post.getPosted());
        ((TextView) view.findViewById(R.id.textViewPost)).setText(post.getPost());
        ((TextView) view.findViewById(R.id.viewPostStats)).setText(postStats);
    }

    //View replies
    void showReplies(PostsResponse post) {

        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();

        Bundle bundle = new Bundle();
        bundle.putString(getActivity().getResources().getString(R.string.param_post_id), Constants.GSON.toJson(post));
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        ReplyFragment replyFragment = new ReplyFragment();
        replyFragment.setArguments(bundle);
        fragmentTransaction.addToBackStack("readMore");
        fragmentTransaction.add(R.id.fragmentHolder, replyFragment);
        fragmentTransaction.commit();

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

//        inflater.inflate(R.menu.menu_main, menu);
//        MenuItem search1 = menu.findItem(R.id.action_search);
//        MenuItem addUser1 = menu.findItem(R.id.action_add_user);
//
//        search1.setVisible(false);
//        addUser1.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case android.R.id.home:

                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();

                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    //Get device id
    public String getDeviceId() {
        return Settings.Secure.getString(getActivity().getApplication().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


}