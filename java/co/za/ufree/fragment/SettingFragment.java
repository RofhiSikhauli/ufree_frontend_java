package co.za.ufree.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import co.za.ufree.R;

/**
 * Created by rofhiwa.sikhauli on 8/4/2015.
 */
public class SettingFragment extends BaseFragment<DashBoardFragment> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.setting_fragment, container, false);


        return view;

    }

}
