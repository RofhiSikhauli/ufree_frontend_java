package co.za.ufree.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import co.za.ufree.R;

/**
 * Created by root on 2015/08/23.
 */
public class AddPeopleFragment extends BaseFragment<DashBoardFragment>  {

    private View view;
    private FragmentManager fragmentManager;
    private Toolbar toolBar;
    private Menu menu;

    /***
     *
     * Start CLass AddPeopleFragment
     *
     *
     * @return AddPeopleFragment Fragment
     */

    public static final AddPeopleFragment start(){

        AddPeopleFragment fragment = new AddPeopleFragment();
        return fragment;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().supportInvalidateOptionsMenu();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        fragmentManager = getFragmentManager();

        view = inflater.inflate(R.layout.add_people_fragment, container, false);

        toolBar = (Toolbar) view.findViewById(R.id.toolBarAddPeople);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolBar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Invite Your Friends");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_light);


        return view;


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        this.menu = menu;
        inflater.inflate(R.menu.menu_main, menu);
        showOverflowMenu(false);
        super.onCreateOptionsMenu(menu, inflater);

    }


    public void showOverflowMenu(boolean showMenu){

        if(menu == null) return;

        menu.setGroupVisible(R.id.chatroom_home_menu, showMenu);
        menu.setGroupVisible(R.id.compose_post_menu, showMenu);
        menu.setGroupVisible(R.id.chat_conversation_menu, showMenu);
        menu.setGroupVisible(R.id.dashboard_menu, showMenu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case android.R.id.home:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                return true;
        }

        return super.onOptionsItemSelected(menuItem);

    }



    @Override
    public void onResume(){
        showOverflowMenu(false);
        super.onResume();

    }


}
