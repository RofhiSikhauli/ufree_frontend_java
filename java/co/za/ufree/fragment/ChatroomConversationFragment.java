package co.za.ufree.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.reflect.TypeToken;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;

import co.za.ufree.R;
import co.za.ufree.adapter.ChatConversationAdapter;
import co.za.ufree.model.ChatroomResponse;
import co.za.ufree.storage.FileStorage;
import co.za.ufree.util.Constants;

/**
 * Created by Rofhiwa on 7/30/2015.
 */
public class ChatroomConversationFragment extends BaseFragment<DashBoardFragment> implements View.OnClickListener {

    private Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private String chatroom, chatroom_description;
    protected ArrayList<ChatroomResponse> valueData;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private EditText editMessage;
    private FloatingActionButton sendMessage;
    private FileStorage storage;

    private String fileName;
    private Menu menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().supportInvalidateOptionsMenu();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_conversation, null);

        toolbar = (Toolbar) view.findViewById(R.id.toolBarChatConvo);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.message_view);
        editMessage = (EditText) view.findViewById(R.id.edit_message);
        sendMessage = (FloatingActionButton) view.findViewById(R.id.send_message);
        sendMessage.setOnClickListener(this);
        chatroom = getArguments().getString(getActivity().getResources().getString(R.string.chatroom_title));
        chatroom_description = getArguments().getString(getActivity().getResources().getString(R.string.chatroom_description));

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(chatroom);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_light);

        storage = new FileStorage();
        fileName = chatroom.replaceAll("\\s", "") + ".txt"; //message file name
        ArrayList<String> messages = storage.ReadFile(fileName); //Get messages

        Log.i(Constants.TAG, messages.toString());

        valueData = Constants.GSON.fromJson(messages.toString(), new TypeToken<ArrayList<ChatroomResponse>>() {
        }.getType());

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new ChatConversationAdapter(valueData, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount());

        return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == sendMessage.getId()) {
            String username = RandomId.nextSessionId();
            username = username.substring(0, 10);
            String message = replaceQuotes(editMessage.getText().toString());

            ChatroomResponse chat = new ChatroomResponse(username, message, new Date().toString());

            Log.d("Message before saved", Constants.GSON.toJson(chat));

            if (!message.isEmpty()) {
                if (valueData.size() > 0) {
                    storage.WriteFile(fileName, "," + Constants.GSON.toJson(chat));
                } else {
                    storage.WriteFile(fileName, Constants.GSON.toJson(chat));
                }

                editMessage.setText("");

                ArrayList<String> messagesJson = storage.ReadFile(fileName); //Get messages

                Log.i(Constants.TAG, messagesJson.toString());

                valueData = Constants.GSON.fromJson(messagesJson.toString(), new TypeToken<ArrayList<ChatroomResponse>>() {}.getType());

                mAdapter = new ChatConversationAdapter(valueData, getActivity()); //get message
                mRecyclerView.setAdapter(mAdapter); //Show message
                mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount());
            }
        }
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        this.menu = menu;
        inflater.inflate(R.menu.menu_main, menu);
        showOverflowMenu(false);
        super.onCreateOptionsMenu(menu, inflater);

    }


    public void showOverflowMenu(boolean showMenu){
        if(menu == null) return;

        menu.setGroupVisible(R.id.chatroom_home_menu, showMenu);
        menu.setGroupVisible(R.id.compose_post_menu, showMenu);
        menu.setGroupVisible(R.id.dashboard_menu, showMenu);

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case android.R.id.home:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroyView() {
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction mTransaction = fragmentManager.beginTransaction();
        Fragment childFragment = this;
        mTransaction.remove(childFragment);
        mTransaction.commit();
        super.onDestroyView();
    }


    //escape quotes
    private String replaceQuotes(String string) {
        String quote = string.replace("\'", "&quo");
        String slashes = quote.replace("\\", "&slashes");

        return slashes;
    }


}


//This is only for testing
//Will be removed when going live
final class RandomId {

    private static SecureRandom random = new SecureRandom();

    public static String nextSessionId() {
        return new BigInteger(130, random).toString(32);
    }
}