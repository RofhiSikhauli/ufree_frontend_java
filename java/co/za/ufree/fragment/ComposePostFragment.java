package co.za.ufree.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.task.AddPostTask;

/**
 * Created by root on 2015/06/17.
 */
public class ComposePostFragment extends BaseFragment<DashBoardFragment>{

    private EditText editTextAddPost;
    private Toolbar composePostToolBar;
    private Application application;
    private DashBoardFragment dashBoard;
    private Menu menu;
    private String postsType;
    private Toolbar toolBarDashboard;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_compose_post, null);

        dashBoard = getBaseParentFragment();
        application = dashBoard.getBaseParentFragment();
        composePostToolBar = (Toolbar) view.findViewById(R.id.composePostToolBar);
        editTextAddPost = (EditText) view.findViewById(R.id.editTextAddPost);
        toolBarDashboard = (Toolbar) application.findViewById(R.id.toolBarDashboard);

        //Add toolbar to this fragment
        ((AppCompatActivity) getActivity()).setSupportActionBar(composePostToolBar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Compose Post");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_light);
        return view;

    }

    public void sendUserPost() {
        dashBoard.hideKeyboard();
        postsType = "0";

        if(!editTextAddPost.getText().toString().isEmpty()) {
            new AddPostTask(getActivity(), response ->
                getBaseParentFragment().getFragmentManager().popBackStack()
            ).execute(Application.getSettings(getActivity().getString(R.string.param_user_id)),
                    application.getDeviceId(), editTextAddPost.getText().toString(), "0,0");
            editTextAddPost.setText("");
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        this.menu = menu;
        inflater.inflate(R.menu.menu_main, menu);
        showOverflowMenu(false);
        super.onCreateOptionsMenu(menu, inflater);

    }


    public void showOverflowMenu(boolean showMenu){
        if(menu == null) return;

        menu.setGroupVisible(R.id.chatroom_home_menu, showMenu);
        menu.setGroupVisible(R.id.compose_post_menu, showMenu);
        menu.setGroupVisible(R.id.chat_conversation_menu, showMenu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int id = menuItem.getItemId();

        switch (id) {

            case android.R.id.home:

                getFragmentManager().popBackStack();
                return true;

            case R.id.action_send:

                sendUserPost();

                return true;

            case R.id.action_clear:

                editTextAddPost.setText("");

                return true;

        }

        return super.onOptionsItemSelected(menuItem);

    }


    @Override
    public void onDetach(){

        super.onDetach();

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolBarDashboard); //Restore dashboard toolbar

    }



}