package co.za.ufree.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.task.LoginTask;
import co.za.ufree.util.Constants;
import co.za.ufree.util.ResponseCodes;
import co.za.ufree.util.ValidationUtils;

/**
 * Created by uFree_Dev_Team on 2015/04/24.
 */
public class LoginFragment extends BaseFragment<Application> implements View.OnClickListener {

    protected Button buttonSignUp, buttonSignIn;
    protected EditText email, password;
    protected TextView errorMessage, spanSignUp, spanSignIn;
    protected LoginButton loginButton;
    CallbackManager callbackManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(Constants.TAG, String.format("%s : %s", "Starting Fragment", getClass().toString()));
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setFragment(this);

        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final ProgressDialog dialog = new ProgressDialog(getActivity());
                dialog.setMessage("Signing in...");
                dialog.show();
                if (Constants.DEBUG) {
                    Log.i("facebook user id ", loginResult.getAccessToken().getUserId());
                }

                new LoginTask(response -> {
                    new LoginTask(loginResponse -> {
                        if (loginResponse.getResponseCode().equals(ResponseCodes.SUCCESS.getCode())) {
                            dialog.dismiss();
                            Application.saveSettings(getActivity().getString(R.string.param_user_id), loginResponse.getUserId());
                            (getBaseParentFragment()).changeFragment(Constants.DASHBOARD_FRAGMENT);
                        }
                    }, getActivity()).execute(loginResult.getAccessToken().getUserId(), loginResult.getAccessToken().getUserId(), (getBaseParentFragment()).getApplicationVersion(), (getBaseParentFragment()).getDeviceId());
                }, getActivity()).execute(loginResult.getAccessToken().getUserId(), loginResult.getAccessToken().getUserId(), getString(R.string.service_register));
            }

            @Override
            public void onCancel() {
                Constants.showToastMessage(getBaseParentFragment(), "User cancelled login action").show();
            }

            @Override
            public void onError(FacebookException exception) {
                Constants.showToastMessage(getBaseParentFragment(), exception.getMessage()).show();
            }
        });

        // Initialise buttons and click handler
        buttonSignUp = (Button) view.findViewById(R.id.buttonSignUp);
        buttonSignUp.setOnClickListener(this);

        buttonSignIn = (Button) view.findViewById(R.id.buttonSignIn);
        buttonSignIn.setOnClickListener(this);

        spanSignUp = (TextView) view.findViewById(R.id.spanSignUp);
        spanSignUp.setOnClickListener(this);

        spanSignIn = (TextView) view.findViewById(R.id.spanSignIn);
        spanSignIn.setOnClickListener(this);

        // Initialise Edit Text and Error message Text View
        email = (EditText) view.findViewById(R.id.editTextEmail);
        password = (EditText) view.findViewById(R.id.editTextPassword);
        errorMessage = (TextView) view.findViewById(R.id.textViewErrorMessage);


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        final Handler handler = new Handler();

        //Change to sign In screen
        if (v.getId() == spanSignIn.getId()) {

            buttonSignUp.setVisibility(View.GONE);
            spanSignIn.setVisibility(View.GONE);
            loginButton.setVisibility(View.GONE);
            buttonSignIn.setVisibility(View.VISIBLE);
            spanSignUp.setVisibility(View.VISIBLE);

            return;

        }

        //Change to Sign up screen
        if (v.getId() == spanSignUp.getId()) {

            buttonSignIn.setVisibility(View.GONE);
            spanSignUp.setVisibility(View.GONE);
            buttonSignUp.setVisibility(View.VISIBLE);
            spanSignIn.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.VISIBLE);

            return;

        }

        //Press login button
        if (v.getId() == buttonSignIn.getId()) {

            //check if email field is not empty
            if (!ValidationUtils.isValidateUserInput(email, "Email can not be empty")) {
                return;
            }

            //check password field if is not empty
            if (!ValidationUtils.isValidateUserInput(password, "Password must not be empty")) {
                return;
            }

            dialog.setMessage("Signing in...");
            dialog.show();

            // Send a login Request
            new LoginTask(response -> {
                if (response.getResponseCode().equals(ResponseCodes.SUCCESS.getCode())) {
                    LoginFragment.this.errorMessage.setVisibility(View.GONE);
                    Application.saveSettings(getActivity().getString(R.string.param_user_id), response.getUserId());
                    (getBaseParentFragment()).changeFragment(Constants.DASHBOARD_FRAGMENT);
                } else if (response.getResponseCode().equals(ResponseCodes.SERVICE_NOT_FOUND.getCode()) ||
                        response.getResponseCode().equals(ResponseCodes.SERVICE_URL_ERROR.getCode())) {
                    Toast.makeText(getActivity(), "Service Error. Please try again later.", Toast.LENGTH_LONG).show();
                } else {

                    LoginFragment.this.errorMessage.setText("Invalid email or password");
                    LoginFragment.this.errorMessage.setVisibility(View.VISIBLE);

                    //Hide error after 5 seconds
                    handler.postDelayed(() -> LoginFragment.this.errorMessage.setVisibility(View.GONE), 5000);
                }
                dialog.dismiss();
            }, getActivity()).execute(email.getText().toString(), password.getText().toString(),
                    (getBaseParentFragment()).getApplicationVersion(),
                    (getBaseParentFragment()).getDeviceId());
        }

        //Press sign up button
        if(v.getId() == buttonSignUp.getId()){

            //check if email field is not empty
            if (!ValidationUtils.isValidateUserInput(email, "Email can not be empty")) {
                return;
            }

            //check password field if is not empty
            if (!ValidationUtils.isValidateUserInput(password, "Password must not be empty")) {
                return;
            }

            //validate email isValidEmail
            if (!ValidationUtils.isValidEmail(email, "Your email is invalid")) {
                return;
            }

            dialog.setMessage("Signing up...");
            dialog.show();

            // Send a Register Request
            new LoginTask(response -> {
                if (response.getResponseCode().equals(ResponseCodes.SUCCESS.getCode())) {

                    //After sigining up, user logging automatically
                    Application.saveSettings(getActivity().getString(R.string.param_user_id), response.getUserId());
                    (getBaseParentFragment()).changeFragment(Constants.DASHBOARD_FRAGMENT);

                } else if (response.getResponseCode().equals(ResponseCodes.SERVICE_NOT_FOUND.getCode()) ||
                        response.getResponseCode().equals(ResponseCodes.SERVICE_URL_ERROR.getCode())) {
                    Toast.makeText(getActivity(), "Service Error. Please try again later.", Toast.LENGTH_LONG).show();
                } else {
                    LoginFragment.this.errorMessage.setText("Failed to create user account");
                    LoginFragment.this.errorMessage.setVisibility(View.VISIBLE);
                    //Hide error after 5 seconds
                    handler.postDelayed(() -> LoginFragment.this.errorMessage.setVisibility(View.GONE), 5000);
                }
                dialog.dismiss();
            }, getActivity()).execute(email.getText().toString(), password.getText().toString(), getString(R.string.service_register));
        }
    }
}
