package co.za.ufree.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import co.za.ufree.dialogs.SearchDialog;
import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.adapter.PostsRecycleViewAdapter;
import co.za.ufree.common.view.SwipeRefreshLayoutBottom;
import co.za.ufree.listener.ScrollPostsListener;
import co.za.ufree.model.BaseResponse;
import co.za.ufree.model.ChatRoom;
import co.za.ufree.model.ChatroomResponse;
import co.za.ufree.model.PostsResponse;
import co.za.ufree.storage.PostsDatabase;
import co.za.ufree.task.LoadPostsTask;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/04/24.
 */
public class PostsFragment extends BaseFragment<DashBoardFragment> implements SwipeRefreshLayoutBottom.OnRefreshListener {

    protected RecyclerView posts;
    private ArrayList<PostsResponse> postsResponses;
    private PostsRecycleViewAdapter postsAdapter;
    private LinearLayoutManager mLayoutManager;
    private String postsType;
    private SwipeRefreshLayout swipeRefresh;
    private View view;
    private Menu menu;
    private FragmentManager fragmentManager;
    private String deviceID;
    private PostsDatabase db;
    private ArrayList<PostsResponse> getSavedPosts;
    public Cursor cursor;
    private ArrayList<PostsResponse> valuePosts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deviceID = getDeviceId();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        fragmentManager = getFragmentManager();

        view = inflater.inflate(R.layout.fragment_posts_layout, container, false);

        posts = (RecyclerView) view.findViewById(R.id.recycleViewYourPosts);
        mLayoutManager = new LinearLayoutManager(getActivity());
        posts.setLayoutManager(mLayoutManager);

        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        swipeRefresh.post(() -> swipeRefresh.setRefreshing(true));

        db = new PostsDatabase(getActivity());

        /***
         *
         * Display posts from database
         *
         * User don't have to wait for new posts
         *
         */

        getSavedPosts = getAllPosts(); //Get saved posts

        if (getSavedPosts != null){

            postsAdapter = new PostsRecycleViewAdapter(getSavedPosts, fragmentManager, getActivity());

            posts.setAdapter(postsAdapter);
            posts.setHasFixedSize(true);

        }

        /***
         * Done loading posts from local DB
         * Now go get new posts from server
         */

        swipeRefresh.setOnRefreshListener(() -> new LoadPostsTask(getActivity(), response -> {
            try {
                postsResponses = Constants.GSON.fromJson((String) response, new TypeToken<ArrayList<PostsResponse>>() {
                }.getType());

                //Save posts to Local DB
                saveNewPostsToLocalDB(Constants.GSON.toJson(postsResponses));

                postsAdapter = new PostsRecycleViewAdapter(postsResponses, fragmentManager, getActivity());

                posts.setAdapter(postsAdapter);
                posts.setHasFixedSize(true);
              posts.addOnScrollListener(new ScrollPostsListener(postsType, posts, mLayoutManager, postsResponses, postsAdapter, getActivity(), deviceID));
            } catch (JsonSyntaxException ex) {
                Toast.makeText(getActivity(), "Unable to load posts", Toast.LENGTH_LONG).show();
            }finally {
                swipeRefresh.setRefreshing(false);
            }
        }).execute(Application.getSettings(getActivity().getString(R.string.param_user_id)), deviceID, "0", "10", postsType));

        mLayoutManager = new LinearLayoutManager(getActivity());
        posts.setLayoutManager(mLayoutManager);

        Application.saveSettings(getActivity().getString(R.string.param_sel_start) + postsType, "0");
        Application.saveSettings(getActivity().getString(R.string.param_sel_limit) + postsType, "10");

        swipeRefresh.post(() -> new LoadPostsTask(getActivity(), response -> {
            try {
                postsResponses = Constants.GSON.fromJson((String) response, new TypeToken<ArrayList<PostsResponse>>() {
                }.getType());

                //Save to DB posts
                saveNewPostsToLocalDB(Constants.GSON.toJson(postsResponses));

                postsAdapter = new PostsRecycleViewAdapter(postsResponses, fragmentManager, getActivity());

                posts.setAdapter(postsAdapter);
                posts.setHasFixedSize(true);
                posts.addOnScrollListener(new ScrollPostsListener(postsType, posts, mLayoutManager, postsResponses, postsAdapter, getActivity(), getDeviceId()));
            } catch (JsonSyntaxException ex) {
                Toast.makeText(getActivity(), "Unable to load posts", Toast.LENGTH_LONG).show();
            } finally {
                swipeRefresh.setRefreshing(false);
            }
        }).execute(Application.getSettings(getActivity().getString(R.string.param_user_id)), deviceID, "0", "10", postsType));

        setHasOptionsMenu(true);
        getActivity().supportInvalidateOptionsMenu();

        return view;
    }

    @Override
    public void onRefresh() {
        Constants.showToastMessage(getActivity(), "Refreshing...").show();
        String start = Application.getSettings(getActivity().getApplication().getString(R.string.param_sel_limit) + postsType);

        new LoadPostsTask(getActivity(), response -> {
            try {
                ArrayList<PostsResponse> responses = Constants.GSON.fromJson((String) response,
                        new TypeToken<ArrayList<PostsResponse>>() {
                        }.getType());
                if (responses.size() > 0) {
                    postsResponses.addAll(responses);
                    postsAdapter.refreshData(postsResponses);
                }
            } catch (JsonSyntaxException ex) {
                BaseResponse baseResponse = Constants.GSON.fromJson(response, BaseResponse.class);
            }

        }).execute(Application.getSettings(getActivity().getApplication().getString(R.string.param_user_id)),
                deviceID, start, String.valueOf(Integer.parseInt(start) + 5),
                String.valueOf(postsType));

        Application.saveSettings(getActivity().getApplication().getString(R.string.param_sel_limit) + postsType, String.valueOf(Integer.parseInt(start) + 5));

    }

    //Get devide id
    public String getDeviceId() {

        try {
            return Settings.Secure.getString(getActivity().getApplication().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }
        catch (NullPointerException e){
            e.printStackTrace();
            return null;
        }

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


        this.menu = menu;
        inflater.inflate(R.menu.menu_main, menu);
        showOverflowMenu(false);
        super.onCreateOptionsMenu(menu, inflater);

    }


    public void showOverflowMenu(boolean showMenu){
        if(menu == null) return;

        menu.setGroupVisible(R.id.chatroom_home_menu, showMenu);
        menu.setGroupVisible(R.id.compose_post_menu, showMenu);
        menu.setGroupVisible(R.id.chat_conversation_menu, showMenu);
        menu.setGroupVisible(R.id.dashboard_menu, true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.action_add_user:
                openAddPeopleFragment();
                return true;

            case R.id.action_search:
                openSearchDialog();
                return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }


    @Override
    public void onPause(){
        Toast.makeText(getActivity(), "Posts Fragment paused", Toast.LENGTH_SHORT).show();
        super.onPause();

    }

    @Override
    public void onResume(){
        Toast.makeText(getActivity(), "Posts Fragment resume", Toast.LENGTH_SHORT).show();
        showOverflowMenu(false);
        super.onResume();

    }



    //This is a repeat method
    //TODO avoid repeating same methods
    public void openSearchDialog() {

        FragmentManager fragmentManager = getFragmentManager();
        SearchDialog searchDialog = new SearchDialog();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(android.R.id.content, searchDialog).addToBackStack(null).commit();

    }


    //OPen add people fragment
    public void openAddPeopleFragment(){

        FragmentManager fragmentManager = getFragmentManager();
        AddPeopleFragment addPeopleFragment = AddPeopleFragment.start();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack("posts");
        transaction.add(R.id.fragmentHolder, addPeopleFragment);
        transaction.commit();

    }


    //Get posts from database and display
    public ArrayList<PostsResponse> getAllPosts(){


        int countRow = db.numCount();

        if(countRow > 0) {

            cursor = db.getAllPosts();

            if (cursor.moveToFirst()) {

                String Post = cursor.getString(cursor.getColumnIndex("Post"));

                Log.i(Constants.TAG, "GET POST FROM LOCAL DB: " + Post);

                postsResponses = Constants.GSON.fromJson((String) Post, new TypeToken<ArrayList<PostsResponse>>() {
                }.getType());


            }

            return postsResponses;

        }

        return null;

    }



    //Save posts to local DB
    public void  saveNewPostsToLocalDB(String post){

        int numRow = db.numCount();

        if(numRow == 0){

            Long insert = db.InsertNewData(post);

            if (insert != -1) {
                Toast.makeText(getActivity(), "Posts saved to local DB", Toast.LENGTH_LONG).show();

            } else if (insert == -1) {
                Toast.makeText(getActivity(), "Error saving posts to local DB", Toast.LENGTH_LONG).show();
            }
        }
    }





}
