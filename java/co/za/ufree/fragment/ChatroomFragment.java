package co.za.ufree.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import co.za.ufree.dialogs.CreateChatroomDialog;
import co.za.ufree.dialogs.SearchDialog;
import co.za.ufree.R;
import co.za.ufree.model.ChatRoom;
import co.za.ufree.storage.ChatroomDatabase;
import co.za.ufree.storage.FileStorage;
import co.za.ufree.activity.Application;
import co.za.ufree.adapter.ChatroomAdapter;
import co.za.ufree.model.ChatroomResponse;
import co.za.ufree.util.Constants;

/**
 * Created by Rofhiwa on 7/30/2015.
 */
public class ChatroomFragment extends BaseFragment<DashBoardFragment> implements View.OnClickListener {

        private Toolbar toolBarDashboard;
        public RecyclerView mRecyclerView;
        public RecyclerView.Adapter mAdapter;
        public RecyclerView.LayoutManager mLayoutManager;
        public ArrayList<ChatroomResponse> valueData;
        public FileStorage storage;
        public ArrayList chatrooms;
        public CreateChatroomDialog createChatroomDialog;
        public ChatroomDatabase db;
        public Cursor cursor;
        private Menu menu;
        private LinearLayout noChatroomFound;
        private Button createChatroomButton;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
            getActivity().supportInvalidateOptionsMenu();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.chatroom_layout, null);

            mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
            noChatroomFound = (LinearLayout) view.findViewById(R.id.noChatroomFound);
            createChatroomButton = (Button) view.findViewById(R.id.createChatroomButton);
            createChatroomButton.setOnClickListener(this);
            createChatroomDialog = new CreateChatroomDialog();

            db = new ChatroomDatabase(getActivity());

            chatrooms = getAllChatrooms(); //Get chatrooms

            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(chatrooms.toString()).getAsJsonArray();

            valueData = new ArrayList<ChatroomResponse>();

            for(JsonElement obj : jArray) {
                ChatroomResponse chatroomList = Constants.GSON.fromJson(obj , ChatroomResponse.class);
                valueData.add(chatroomList);
            }

            mRecyclerView.setHasFixedSize(true);

            // Linear layout manager
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            FragmentManager fragmentManager = getFragmentManager();

            //Call adapter
            mAdapter = new ChatroomAdapter(valueData, getActivity(),  fragmentManager, noChatroomFound);
            mRecyclerView.setAdapter(mAdapter);

            return view;


        }

        @Override
        public void onClick(View v){
            int id = v.getId();
            if(id == R.id.createChatroomButton){
                createChatroomDialog.show(getFragmentManager(), "create_chatroom");
            }
        }

        //Get chatrooms from database and display
        public ArrayList getAllChatrooms(){

            cursor = db.getAllData();
            ArrayList<String> jsonArr = new ArrayList<>();

            if (cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(cursor.getColumnIndex("id"));
                    String title = cursor.getString(cursor.getColumnIndex("title"));
                    String description = cursor.getString(cursor.getColumnIndex("description"));
                    int radius = cursor.getInt(cursor.getColumnIndex("radius"));

                    Log.i(Constants.TAG,"Create chat room : " + Constants.GSON.toJson(new ChatRoom(id,title,description,radius)));
                    jsonArr.add(Constants.GSON.toJson(new ChatRoom(id,title,description,radius)));

                } while (cursor.moveToNext());
            }

            return  jsonArr;

        }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


        this.menu = menu;
        inflater.inflate(R.menu.menu_main, menu);
        showOverflowMenu(false);
        super.onCreateOptionsMenu(menu, inflater);

    }


    public void showOverflowMenu(boolean showMenu){
        if(menu == null) return;

        menu.setGroupVisible(R.id.dashboard_menu, showMenu);
        menu.setGroupVisible(R.id.compose_post_menu, showMenu);
        menu.setGroupVisible(R.id.chat_conversation_menu, showMenu);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.action_create:
                createChatroomDialog.show(getFragmentManager(), "create_chatroom");
                return true;

            case R.id.action_search:
                openSearchDialog();
                return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }



    //Open search
    public void openSearchDialog() {

        FragmentManager fragmentManager = getFragmentManager();
        SearchDialog searchDialog = new SearchDialog();

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        transaction.add(android.R.id.content, searchDialog).addToBackStack(null).commit();

    }


}
