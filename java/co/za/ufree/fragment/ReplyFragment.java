package co.za.ufree.fragment;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.adapter.RepliesRecycleViewAdapter;
import co.za.ufree.model.PostsResponse;
import co.za.ufree.task.CommentPostTask;
import co.za.ufree.task.LoadPostRepliesTask;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/05/28.
 */
public class ReplyFragment extends BaseFragment<DashBoardFragment> {
    private FloatingActionButton buttonReply;
    private EditText editTextAddPost;
    private RecyclerView listPostReplies;
    private ProgressBar loadReplyProgressBar;
    private LinearLayout noReplyFound;
    public Toolbar replyToolBar;
    private Menu menu;
    private Toolbar toolBarDashboard;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().supportInvalidateOptionsMenu();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_reply, null);

        PostsResponse post = Constants.GSON.fromJson(getArguments().getString(getActivity().getResources().getString(R.string.param_post_id)), PostsResponse.class);

        loadReplyProgressBar = (ProgressBar) view.findViewById(R.id.loadReplyProgressBar);
        noReplyFound = (LinearLayout) view.findViewById(R.id.noReplyFound);
        listPostReplies = (RecyclerView) view.findViewById(R.id.listPostReplies);
        replyToolBar = (Toolbar) view.findViewById(R.id.replyToolBar);
        listPostReplies.setLayoutManager(new LinearLayoutManager(getActivity()));

        int replyCount = Integer.parseInt(post.getReplyCount());

        //Add toolbar to this fragment
        ((AppCompatActivity)getActivity()).setSupportActionBar(replyToolBar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Replies (" + replyCount + ")");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_light);

                //Check if there is any reply in the post
                if(replyCount == 0){

                    loadReplyProgressBar.setVisibility(View.GONE);
                    noReplyFound.setVisibility(View.VISIBLE);

                }

                new LoadPostRepliesTask(getActivity(), response -> {

                    try {
                        ArrayList<PostsResponse.PostReply> replies = Constants.GSON.fromJson((String) response, new TypeToken<ArrayList<PostsResponse.PostReply>>() {
                        }.getType());

                        loadReplyProgressBar.setVisibility(View.GONE);
                        listPostReplies.setAdapter(new RepliesRecycleViewAdapter(replies));
                    } catch (JsonSyntaxException ex) {
                        // TODO handle json error here
            }
        }).execute(getDeviceId(), Application.getSettings(getActivity().getString(R.string.param_user_id)), post.getId());

        editTextAddPost = (EditText) view.findViewById(R.id.editTextAddPost);
        buttonReply = (FloatingActionButton) view.findViewById(R.id.buttonReply);


        //User is about to press send reply
        buttonReply.setOnClickListener(event -> {

            //Now start by checking if reply editText is not empty before submitting to the server
            if(!editTextAddPost.getText().toString().isEmpty()) {

                new CommentPostTask(getActivity(), response -> {

                    noReplyFound.setVisibility(View.GONE);

                    int totalReplies = replyCount + 1;

                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Replies (" + totalReplies + ")");

                    new LoadPostRepliesTask(getActivity(), getReplies -> {

                        try {
                            ArrayList<PostsResponse.PostReply> replies = Constants.GSON.fromJson((String) getReplies, new TypeToken<ArrayList<PostsResponse.PostReply>>() {
                            }.getType());

                            listPostReplies.setAdapter(new RepliesRecycleViewAdapter(replies));

                        } catch (JsonSyntaxException ex) {
                            // TODO handle json error here
                        }
                    }).execute(getDeviceId(), Application.getSettings(getActivity().getString(R.string.param_user_id)), post.getId());


                }).execute(Application.getSettings(getActivity().getString(R.string.param_user_id)), getDeviceId(), post.getId(), editTextAddPost.getText().toString());


                editTextAddPost.setText(""); // Reply has been sent now clear editText

            }

        });


        return view;

    }

    public String getDeviceId() {
        return Settings.Secure.getString(getActivity().getApplication().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

//        inflater.inflate(R.menu.menu_main, menu);
//        this.menu = menu;
//        MenuItem search = menu.findItem(R.id.action_search);
//        MenuItem addUser = menu.findItem(R.id.action_add_user);
//
//        search.setVisible(false);
//        addUser.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        int id = menuItem.getItemId();

        if(id == android.R.id.home){

            FragmentManager fm = getActivity().getSupportFragmentManager();
            fm.popBackStack();

            return  true;
        }

        return super.onOptionsItemSelected(menuItem);

    }



}

