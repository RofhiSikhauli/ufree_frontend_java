package co.za.ufree.fragment;

import android.support.v4.app.Fragment;

/**
 * Created by uFree_Dev_Team on 2015/04/26.
 */
public class BaseFragment<T> extends Fragment {

    protected T parentFragment;

    public BaseFragment setParentFragment(T object) {
        parentFragment = object;
        return this;
    }

    public T getBaseParentFragment() {
        return parentFragment;
    }
}
