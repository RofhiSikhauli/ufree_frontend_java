package co.za.ufree.adapter;

/**
 * Created by Rofhiwa on 7/30/2015.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.dialogs.ChatroomOptionsDialog;
import co.za.ufree.storage.FileStorage;
import co.za.ufree.fragment.ChatroomConversationFragment;
import co.za.ufree.model.ChatroomResponse;

public class ChatroomAdapter extends RecyclerView.Adapter<ChatroomAdapter.ViewHolder> {

    private ArrayList<ChatroomResponse> mDataset;
    private Context context;
    private FileStorage storage;
    protected FragmentManager fragmentManager;
    private LinearLayout noChatroomFound;
    private ChatroomOptionsDialog chatroomOptionsDialog;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public LinearLayout mainView;
        public TextView viewText;
        public TextView viewIcon;
        public TextView viewRadius;
        public TextView viewDescr;

        public ViewHolder(View v) {
            super(v);
            mainView = (LinearLayout) v.findViewById(R.id.view_chatroom_cover);
            viewText = (TextView) v.findViewById(R.id.chatroom_name);
            viewIcon = (TextView) v.findViewById(R.id.chatroom_icon);
            viewRadius = (TextView) v.findViewById(R.id.chatroom_distance);
            viewDescr = (TextView) v.findViewById(R.id.chatroom_descr);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatroomAdapter(ArrayList<ChatroomResponse> myDataset, Context mContext, FragmentManager fragmentTransaction, LinearLayout noChatroomFound) {
        this.mDataset = myDataset;
        this.context = mContext;
        this.fragmentManager = fragmentTransaction;
        this.noChatroomFound = noChatroomFound;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public ChatroomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatroom_lists, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(getItemCount() > 0) {

            ChatroomResponse data = mDataset.get(position);

            noChatroomFound.setVisibility(View.GONE);

            final String title = data.getTitle();
            final String description = data.getDescription();
            final int radius = data.getRadius();

            String iconText = title.substring(0, 1).toUpperCase();

            holder.viewIcon.setText(iconText);
            holder.viewText.setText(title);
            holder.viewRadius.setText(radius + " km");
            holder.viewDescr.setText(description);

            //On single click go to chatroom conversation
            holder.mainView.setOnClickListener((View v) -> {

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                Bundle bundle = new Bundle();
                bundle.putString("chatroom_title", title);
                bundle.putString("chatroom_description", description);
                ChatroomConversationFragment chatConversation = new ChatroomConversationFragment();
                chatConversation.setArguments(bundle);
                fragmentTransaction.addToBackStack("chat");
                fragmentTransaction.add(R.id.fragmentHolder, chatConversation);
                fragmentTransaction.commit();

            });

            //On long click open menu
            holder.mainView.setOnLongClickListener((View v) -> {

                chatroomOptionsDialog = new ChatroomOptionsDialog();
                chatroomOptionsDialog.show(fragmentManager, "chatroom_option_menu");
                return true;

            });



        }

    }


}
