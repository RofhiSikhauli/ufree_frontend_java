package co.za.ufree.adapter;

/**
 * Created by Rofhiwa on 7/30/2015.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.storage.FileStorage;
import co.za.ufree.fragment.ChatroomConversationFragment;
import co.za.ufree.model.ChatroomResponse;

public class UpdatesAdapter extends RecyclerView.Adapter<UpdatesAdapter.ViewHolder> {

    private ArrayList<ChatroomResponse> mDataset;
    private Context context;
    private FileStorage storage;
    protected FragmentManager fragmentManager;
    private LinearLayout noUpdatesFound;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView updatesBody;
        public TextView updatesTime;

        public ViewHolder(View v) {
            super(v);
            updatesBody = (TextView) v.findViewById(R.id.updatesBody);
            updatesTime = (TextView) v.findViewById(R.id.updatesTime);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public UpdatesAdapter(ArrayList<ChatroomResponse> myDataset, Context mContext, FragmentManager fragmentTransaction, LinearLayout noUpdatesFound) {
        this.mDataset = myDataset;
        this.context = mContext;
        this.fragmentManager = fragmentTransaction;
        this.noUpdatesFound = noUpdatesFound;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public UpdatesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.updates_lists, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(getItemCount() > 0) {

            ChatroomResponse data = mDataset.get(position);

            noUpdatesFound.setVisibility(View.GONE);

            final String updates = data.getUpdates();
            final String updateTime = data.getTime();

            holder.updatesBody.setText(updates);
            holder.updatesTime.setText(updateTime);

        }

    }


}
