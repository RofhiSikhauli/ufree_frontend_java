package co.za.ufree.adapter;

import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Vector;

import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.fragment.BaseFragment;
import co.za.ufree.fragment.ChatroomFragment;
import co.za.ufree.fragment.DashBoardFragment;
import co.za.ufree.fragment.PostsFragment;
import co.za.ufree.fragment.SettingFragment;
import co.za.ufree.fragment.UpdateFragment;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/04/24.
 */
public class PageAdapter extends FragmentStatePagerAdapter {

    protected DashBoardFragment application;
    private Context mContext;
    private ViewPager viewPager;
    private Bundle bundle;

    final int[] tabIcons = {
            android.R.drawable.stat_notify_chat,
            R.drawable.ic_group_light,
            R.drawable.ic_updates,
            R.drawable.ic_setting
    };

    public PageAdapter(FragmentManager fragmentManager, Context context, ViewPager viewPager) {
        super(fragmentManager);
        this.mContext = context;
        this.viewPager = viewPager;
    }



    @Override
    public Fragment getItem(int position) {

        switch (position){

            case 0:
                return Fragment.instantiate(mContext, PostsFragment.class.getName());

            case 1:
                return Fragment.instantiate(mContext, ChatroomFragment.class.getName());

            case 2:
                return Fragment.instantiate(mContext, UpdateFragment.class.getName());

            case 3:
                return Fragment.instantiate(mContext, SettingFragment.class.getName());

        }

        return null;

    }

    @Override
    public int getCount() {
        return 4;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        //Add icons to the tab instead of tab titles
        Drawable image = ResourcesCompat.getDrawable(mContext.getResources(), tabIcons[position], null);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString spannableString = new SpannableString(" ");
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        spannableString.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;

    }
}
