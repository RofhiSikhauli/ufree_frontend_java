package co.za.ufree.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.activity.Application;
import co.za.ufree.dialogs.PostModifyDialog;
import co.za.ufree.fragment.DashBoardFragment;
import co.za.ufree.fragment.ReadMoreFragment;
import co.za.ufree.fragment.ReplyFragment;
import co.za.ufree.model.PostsResponse;
import co.za.ufree.storage.PostsDatabase;
import co.za.ufree.task.LikePostTask;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/05/13.
 */
public class PostsRecycleViewAdapter extends RecyclerView.Adapter<PostsRecycleViewAdapter.Post> {
    protected ArrayList<PostsResponse> posts;
    protected Context context;
    protected FragmentManager fragmentManager;
    private PostsDatabase db;

    public PostsRecycleViewAdapter(ArrayList<PostsResponse> posts, FragmentManager fragmentManager, Context mContext) {
        this.posts = posts;
        this.fragmentManager = fragmentManager;
        this.context = mContext;
        this.db = new PostsDatabase(mContext);
    }

    @Override
    public Post onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_post_cardview, parent, false);
        return new Post(v);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(Post holder, int position) {
        // Get post object and set post fields
        PostsResponse post = posts.get(position);

        setReadMorePost(holder.postText, post);
        holder.viewPostTime.setText(post.getPosted());

        int this_user_id = Integer.parseInt(Application.getSettings(context.getString(R.string.param_user_id)));
        int post_user_id = Integer.parseInt(post.getUserId());
        int post_id = Integer.parseInt(post.getId());
        int totalUpVotesCount = Integer.parseInt(post.getLikesCount());
        int totalDownVotesCount = Integer.parseInt(post.getDislikeCount());
        int totalReplyCount = Integer.parseInt(post.getReplyCount());
        String postStats = "";


        //Check if its current user's posts
        if(this_user_id == post_user_id){
            holder.postOptions.setVisibility(View.VISIBLE);
        }else{
            holder.postOptions.setVisibility(View.GONE);
        }

        //Get upvotes plural and singular ex (1 vote, 2 votes)
        if(totalUpVotesCount == 1){
            postStats += totalUpVotesCount + " upvote  ";
        }else if(totalUpVotesCount > 1){
            postStats += totalUpVotesCount + " upvotes  ";
        }


        //Get downvotes plural and singular ex (1 vote, 2 votes)
        if(totalDownVotesCount == 1){
            postStats += totalDownVotesCount + " downvote  ";
        }else if(totalDownVotesCount > 1){
            postStats += totalDownVotesCount + " downvotes  ";
        }

        //Get votes plural and singular ex (1 reply, 2 replies)
        if(totalReplyCount == 1){
            postStats += totalReplyCount + " reply";
        }else if(totalReplyCount > 1){
            postStats += totalReplyCount + " replies";
        }

        //set Post stats
        holder.viewPostStats.setText(postStats);

        if (post.getFlags().getLike().equals(context.getResources().getString(R.string.val_off))) {

            holder.textViewLike.setTextColor(context.getResources().getColor(R.color.activePostStatsTextColor)); //Restore text color

            holder.textViewLike.setOnClickListener(click -> new LikePostTask(context, response ->
                    updatePost(position, response, posts)
            ).execute(Application.getSettings(context.getString(R.string.param_user_id)), Application.getSettings(context.getString(R.string.setting_device_id)), post.getId(), context.getResources().getString(R.string.val_on)));

        }else if(post.getFlags().getLike().equals(context.getResources().getString(R.string.val_on))) {

            holder.textViewLike.setTextColor(context.getResources().getColor(R.color.inactivePostStatsTextColor)); //Change text color if post liked

        }

        if (post.getFlags().getDislike().equals(context.getResources().getString(R.string.val_off))) {

            holder.textViewDislike.setTextColor(context.getResources().getColor(R.color.activePostStatsTextColor)); //Restore text color

            holder.textViewDislike.setOnClickListener(click -> new LikePostTask(context, response ->
                    updatePost(position, response, posts)
            ).execute(Application.getSettings(context.getString(R.string.param_user_id)), Application.getSettings(context.getString(R.string.setting_device_id)), post.getId(), context.getResources().getString(R.string.val_off)));

        }else if(post.getFlags().getDislike().equals(context.getResources().getString(R.string.val_on))) {

            holder.textViewDislike.setTextColor(context.getResources().getColor(R.color.inactivePostStatsTextColor)); //Change text color if post disliked

        }

        //Show reply
        holder.textViewReply.setOnClickListener(view -> showReplies(post));

        //Show post options only on your post
        holder.postOptions.setOnClickListener(view -> openPostOption(post_id, position));


    }

    //Show replies
    void showReplies(PostsResponse post) {
        Bundle bundle = new Bundle();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        bundle.putString(context.getResources().getString(R.string.param_post_id), Constants.GSON.toJson(post));
        ReplyFragment replyFragment = new ReplyFragment();
        replyFragment.setArguments(bundle);
        fragmentTransaction.addToBackStack("posts");
        fragmentTransaction.add(R.id.fragmentHolder, replyFragment);
        fragmentTransaction.commit();
    }

    //Read all posts
    void ReadAllPost(PostsResponse post) {
        Bundle bundle = new Bundle();
        bundle.putString(context.getResources().getString(R.string.param_post_id), Constants.GSON.toJson(post));
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ReadMoreFragment readMoreFragment = new ReadMoreFragment();
        readMoreFragment.setArguments(bundle);
        fragmentTransaction.addToBackStack("posts");
        fragmentTransaction.add(R.id.fragmentHolder, readMoreFragment);
        fragmentTransaction.commit();

    }

    //Open posts option
    void openPostOption(int post_id, int post_pos){
        PostModifyDialog postModifyDialog = new PostModifyDialog();
        postModifyDialog.setParams(post_id, post_pos);
        postModifyDialog.show(fragmentManager, "posts_option");
    }


    //Read more button on click
    void setReadMorePost(TextView textView, PostsResponse post) {
        // Show read more if post has more than eight line or has more than 225 characters

        final TextView showPosts = textView;

        String[] postLines = post.getPost().split(System.getProperty("line.separator"));
        if (postLines.length > 8 || post.getPost().length() > 250) {
            String stringPost = postLines.length > 6 ? postLines[0] + "\n" + postLines[1] + "\n" + postLines[2] + "\n" + postLines[3] + "\n" + postLines[4] + "\n" + postLines[5] : post.getPost().substring(0, 250);
            SpannableString spannableString = new SpannableString(stringPost + "...Continue reading");
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    showPosts.setText(post.getPost());
                }
            };

            spannableString.setSpan(clickableSpan, spannableString.length() - 19, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(spannableString);
            textView.setMovementMethod(LinkMovementMethod.getInstance());

        } else {
            textView.setText(post.getPost());
        }
    }

    void updatePost(int position, Object response, ArrayList<PostsResponse> posts) {
        // update post after user selects like or dislike
        try {
            posts.remove(position);
            PostsResponse responsePost = ((ArrayList<PostsResponse>) Constants.GSON.fromJson((String) response, new TypeToken<ArrayList<PostsResponse>>() {
            }.getType())).get(0);
            if (responsePost != null) {
                posts.add(position, responsePost);
                notifyDataSetChanged();
            }
        } catch (JsonSyntaxException ex) {

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    // IMPORTANT : synchronously refresh posts with disrupting the currently viewed posts
    public synchronized void refreshData(ArrayList<PostsResponse> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }


    // View place holder for each post
    public static class Post extends RecyclerView.ViewHolder {

        TextView postText, viewPostTime, viewPostStats, textViewLike, textViewDislike, textViewReply;
        LinearLayout postOptions;

        Post(View parent) {
            super(parent);
            this.postText = (TextView) parent.findViewById(R.id.textViewPost);
            this.viewPostTime = (TextView) parent.findViewById(R.id.viewPostTime);
            this.postOptions = (LinearLayout) parent.findViewById(R.id.postOptions);
            this.viewPostStats = (TextView) parent.findViewById(R.id.viewPostStats);
            this.textViewLike = (TextView) parent.findViewById(R.id.textViewLike);
            this.textViewDislike = (TextView) parent.findViewById(R.id.textViewDislike);
            this.textViewReply = (TextView) parent.findViewById(R.id.textViewReply);
        }
    }
}
