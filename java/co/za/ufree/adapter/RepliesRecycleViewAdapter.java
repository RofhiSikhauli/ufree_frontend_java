package co.za.ufree.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.model.PostsResponse;
import co.za.ufree.util.Constants;

/**
 * Created by uFree_Dev_Team on 2015/06/14.
 */
public class RepliesRecycleViewAdapter extends RecyclerView.Adapter<RepliesRecycleViewAdapter.Reply> {

    private ArrayList<PostsResponse.PostReply> replies;

    public RepliesRecycleViewAdapter(ArrayList<PostsResponse.PostReply> replies) {
        Log.i(Constants.TAG, "Starting Recycle view list");
        this.replies = replies;
    }

    @Override
    public Reply onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_reply, parent, false);

        Reply reply = new Reply(view);
        return reply;
    }

    @Override
    public void onBindViewHolder(Reply holder, int position) {
        PostsResponse.PostReply postReply = replies.get(position);
        holder.replyText.setText(postReply.getReply());
        holder.viewReplyTime.setText(postReply.getReplyTime());
    }

    @Override
    public int getItemCount() {
        return replies.size();
    }

    public static class Reply extends RecyclerView.ViewHolder {
        TextView replyText, viewReplyTime;

        public Reply(View itemView) {
            super(itemView);
            replyText = (TextView) itemView.findViewById(R.id.replyText);
            viewReplyTime = (TextView) itemView.findViewById(R.id.viewReplyTime);

        }
    }
}
