package co.za.ufree.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.model.PostsResponse;

/**
 * Created by uFree_Dev_Team on 2015/05/02.
 */
public class PostsAdapter extends BaseAdapter {
    protected ArrayList<PostsResponse> posts;
    protected Context context;

    public PostsAdapter(ArrayList<PostsResponse> posts, Context context) {
        this.posts = posts;
        this.context = context;
    }

    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public Object getItem(int position) {
        return posts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                    inflate(R.layout.fragment_post_cardview, null);
        }

        TextView postText = (TextView)convertView.findViewById(R.id.textViewPost);
        postText.setText(((PostsResponse)getItem(position)).getPost());

        return convertView;
    }

    public synchronized void refreshData(ArrayList<PostsResponse> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }
}
