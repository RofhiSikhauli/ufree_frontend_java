package co.za.ufree.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.model.ChatroomResponse;

/**
 * Created by root on 2015/07/08.
 */
public class ChatConversationAdapter extends RecyclerView.Adapter<ChatConversationAdapter.ViewHolder> {

    private ArrayList<ChatroomResponse> mDataset;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case

        public TextView textViewUsername, textViewChatMessage, textViewChatTime;
        public LinearLayout MessageContainer, chatMainCover;

        public ViewHolder(View v) {
            super(v);
            chatMainCover = (LinearLayout) v.findViewById(R.id.chatMainCover);
            MessageContainer = (LinearLayout) v.findViewById(R.id.MessageContainer);
            textViewUsername = (TextView) v.findViewById(R.id.textViewUsername);
            textViewChatMessage = (TextView) v.findViewById(R.id.textViewChatMessage);
            textViewChatTime = (TextView) v.findViewById(R.id.textViewChatTime);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatConversationAdapter(ArrayList<ChatroomResponse> myDataset, Context mContext) {
        mDataset = myDataset;
        context = mContext;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ChatConversationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatroom_message_bubble_layout, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ChatroomResponse data = mDataset.get(position);

        final String username = data.getUsername();
        final String message = data.getTextMessage();
        final String time = data.getTime();

        LinearLayout.LayoutParams linearLayout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        LinearLayout.LayoutParams textViewTime = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        //This is for testing, I will remove it and check with user_id
        if(position % 2 == 0) {


            holder.chatMainCover.setGravity(Gravity.RIGHT);
            holder.MessageContainer.setBackgroundResource(R.drawable.chatroom_message_bubble_style_incoming);

            linearLayout.setMargins(30, 0, 0, 0);
            holder.MessageContainer.setLayoutParams(linearLayout);

            holder.textViewChatTime.setGravity(Gravity.RIGHT);

            textViewTime.gravity = Gravity.RIGHT;
            textViewTime.setMargins(0, 0, 5, 0);
            holder.textViewChatTime.setLayoutParams(textViewTime);

            holder.textViewUsername.setTextColor(context.getResources().getColor(R.color.lightGrey));
            holder.textViewChatMessage.setTextColor(context.getResources().getColor(R.color.white));

        }

        holder.textViewUsername.setText(username);
        holder.textViewChatMessage.setText(restoreQuotes(message));
        holder.textViewChatTime.setText(time);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }



    //escape quotes
    private String restoreQuotes(String string){

        String quote = string.replace("&quo", "\'");
        String slashes = quote.replace("&slashes", "\\");

        return slashes;

    }



}
