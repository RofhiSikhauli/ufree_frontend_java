package co.za.ufree.storage;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Rofhiwa on 2015/07/24.
 */
public class FileStorage {

    private String EXTERNAL_PATH = Environment.getExternalStorageDirectory().getAbsolutePath(); //external path
    private String INTERNAL_PATH = Environment.getDataDirectory().getAbsolutePath(); //internal path
    private String APP_NAME = "/uFree/";
    private String DATA_DIR = "/Data/";
    public String MAIN_DIR = EXTERNAL_PATH + APP_NAME + DATA_DIR;
    private File path, getFile, setFile;
    private FileWriter fileWriter;
    private FileReader fileReader;
    public ArrayList output;
    private BufferedReader bufferedReader;

    //Construct
    public FileStorage() {


    }

    //Create or write file
    public boolean WriteFile(String fileName, String data) {

        path = new File(MAIN_DIR); //data directory

        boolean success = true;

        if (!path.exists()) {
            path.mkdirs();
        }

        try {

            setFile = new File(path, fileName);
            fileWriter = new FileWriter(setFile, true);
            fileWriter.append(data);
            fileWriter.flush();
            fileWriter.close();
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }

        return success;

    }


    //Read file
    public ArrayList<String> ReadFile(String fileName) {

        getFile = new File(MAIN_DIR, fileName);
        output = new ArrayList<>();


        try {
            fileReader = new FileReader(getFile);

            bufferedReader = new BufferedReader(fileReader);

            String Line = "";

            while ((Line = bufferedReader.readLine()) != null) {

                output.add(Line);

            }

            fileReader.close();

        } catch (FileNotFoundException e) {
            WriteFile(fileName, ""); //File not found then create it
        } catch (IOException e) {

        }

        return output;

    }


}
