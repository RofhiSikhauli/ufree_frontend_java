package co.za.ufree.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by root on 2015/08/22.
 */
public class DBConfig  extends SQLiteOpenHelper {

    //Database config
    protected static final int DATABASE_VERSION = 4;
    protected static final String DATABASE_NAME = "ufreeLocalDatabase";

    //Auto increment id
    protected static final String KEY_ID = "id";

    //Posts table config
    public static final String POSTS_TABLE_NAME = "posts";
    protected static final String KEY_POST = "Post";

    //Chatrooms table config
    public static final String CHATROOMS_TABLE_NAME = "chatrooms";
    protected static final String KEY_TITLE = "title";
    protected static final String KEY_DESCRIPTION = "description";
    protected static final String KEY_RADIUS = "radius";

    //Create chatroom table query
    protected static final String CHATROOMS_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + CHATROOMS_TABLE_NAME + " ("
            + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_TITLE + " TEXT NOT NULL, "
            + KEY_DESCRIPTION + " TEXT NOT NULL, "
            + KEY_RADIUS + " INTEGER NOT NULL);";


    //Create posts table query
    protected static final String POSTS_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + POSTS_TABLE_NAME + " ("
            + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_POST + " TEXT NOT NULL);";



    //Construct
    public DBConfig(Context context){

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(POSTS_TABLE_CREATE);
        db.execSQL(CHATROOMS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int NewVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + POSTS_TABLE_NAME + ";");
        db.execSQL("DROP TABLE IF EXISTS " + CHATROOMS_TABLE_CREATE + ";");

        onCreate(db);

    }



}
