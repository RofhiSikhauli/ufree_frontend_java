package co.za.ufree.storage;

/**
 * Created by Rofhiwa on 7/30/2015.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class ChatroomDatabase extends DBConfig {


   public ChatroomDatabase(Context context){

       super(context);

    }



    //Insert data
    public Long InsertNewData(String title, String description, int radius) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TITLE, title); // Chatroom title
        values.put(KEY_DESCRIPTION, description); // Chatroom description
        values.put(KEY_RADIUS, radius); // Chatroom description

        // Inserting new row
        return db.insert(CHATROOMS_TABLE_NAME, null, values);

    }

    //Get data by keyword
    public Cursor getCertainData(String keyword) {

        SQLiteDatabase db = this.getReadableDatabase();

        String selQuery = "SELECT * FROM " + CHATROOMS_TABLE_NAME + " WHERE " + KEY_TITLE + " =? OR " + KEY_ID + " =?;";

        Cursor cursor = db.rawQuery(selQuery, new String[]{String.valueOf(keyword), String.valueOf(keyword)});

        return cursor;

    }


    //Get data by keyword
    public Cursor getAllData() {


        SQLiteDatabase db = this.getReadableDatabase();

        try {
            String countQuery = "SELECT  * FROM " + CHATROOMS_TABLE_NAME + ";";
            Cursor cursor = db.rawQuery(countQuery, null);

            return cursor;
        }
        catch (SQLiteException e){
            db.execSQL(CHATROOMS_TABLE_CREATE);
        }

        return null;
    }


    //Update data
    public int updateData(int id, String title, String description, int radius) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TITLE, title); // Chatroom title
        values.put(KEY_DESCRIPTION, description); // Chatroom description
        values.put(KEY_RADIUS, radius); // Chatroom description

        // updating row
        return db.update(CHATROOMS_TABLE_NAME, values, KEY_ID + " = ?", new String[]{String.valueOf(id)});

    }


    //Delete data
    public int deleteData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(CHATROOMS_TABLE_NAME, KEY_ID + " = ?", new String[]{String.valueOf(id)});

    }

    //Count row
    public int numCount() {
        String countQuery = "SELECT  * FROM " + CHATROOMS_TABLE_NAME + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        // return count
        return cursor.getCount();

    }

}