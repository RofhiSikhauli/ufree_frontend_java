package co.za.ufree.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

/**
 * Created by root on 2015/08/22.
 */
public class PostsDatabase extends DBConfig {

    public PostsDatabase(Context context){

        super(context);

    }

    //Insert data
    public Long InsertNewData(String post) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_POST, post);

        //First truncate table, only have to save 5 posts
        deleteAllData();

        // Inserting new row
        return db.insert(POSTS_TABLE_NAME, null, values);

    }



    //Get data by keyword
    public Cursor getAllPosts() {


        SQLiteDatabase db = this.getReadableDatabase();

        try{
            String countQuery = "SELECT  * FROM " + POSTS_TABLE_NAME + ";";
            Cursor cursor = db.rawQuery(countQuery, null);
            return cursor;
        }
        catch (SQLiteException e){
            db.execSQL(POSTS_TABLE_CREATE);
        }

        return null;

    }



    //Delete data
    public void deleteAllData() {

        SQLiteDatabase db = this.getWritableDatabase();

        String deleteQuery = "DELETE FROM " + POSTS_TABLE_NAME + ";";

        db.execSQL(deleteQuery); //Truncate table

    }

    //Count row
    public int numCount() {
        String countQuery = "SELECT  * FROM " + POSTS_TABLE_NAME + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        // return count
        return cursor.getCount();

    }


}
