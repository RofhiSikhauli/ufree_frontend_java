package co.za.ufree.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import co.za.ufree.R;

/**
 * Created by Rofhiwa on 7/29/2015.
 */
public class SearchDialog extends DialogFragment {

    private Dialog dialog;
    private LinearLayout closeSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.search_view_layout, container, false);

        closeSearch =  (LinearLayout) v.findViewById(R.id.closeSearchDialog);

        closeSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    getFragmentManager().popBackStack();
                } catch (Exception e){
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }

            }
        });

        return v;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;

    }



}
