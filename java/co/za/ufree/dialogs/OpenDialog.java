package co.za.ufree.dialogs;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

/**
 * Created by root on 2015/07/26.
 */
public class OpenDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstance){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Sure this the alert dialog")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id){
                        Toast.makeText(getActivity(), "You clicked nop", Toast.LENGTH_LONG).show();
                    }

         });



        return builder.create();

    }


}
