package co.za.ufree.dialogs;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import co.za.ufree.R;

/**
 * Created by root on 2015/07/26.
 */
public class ChatroomOptionsDialog extends DialogFragment {

    private int post_id;
    private int post_pos;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(R.array.chatroom_options_dialog_list, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which){

                            case 0: //Hide

                                Toast.makeText(getActivity(), "You clicked hide button", Toast.LENGTH_SHORT).show();
                                return;

                            case 1: //Remove

                                Toast.makeText(getActivity(), "You clicked remove button", Toast.LENGTH_SHORT).show();
                                return;

                            case 2: //Cancel

                                Toast.makeText(getActivity(), "You clicked cancel button", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                                return;

                        }

                    }

                });
        return builder.create();

    }


    public void setParams(int post_id, int post_pos){
        this.post_id = post_id;
        this.post_pos = post_pos;
    }


    private int getPostId(){

        return post_id;

    }

    private int getPostPos(){

        return post_pos;

    }


}
