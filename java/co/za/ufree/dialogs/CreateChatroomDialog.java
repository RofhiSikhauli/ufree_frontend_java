package co.za.ufree.dialogs;

import android.database.Cursor;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import co.za.ufree.R;
import co.za.ufree.adapter.ChatroomAdapter;
import co.za.ufree.model.ChatroomResponse;
import co.za.ufree.storage.ChatroomDatabase;
import co.za.ufree.util.Constants;

/**
 * Created by root on 2015/07/26.
 */
public class CreateChatroomDialog extends DialogFragment{

    private SeekBar seekBar;
    private TextView showLocRadius, chatroom_name, chatroom_descr;
    public ChatroomDatabase db;
    public RecyclerView mRecyclerView;
    public RecyclerView.Adapter mAdapter;
    public LinearLayout noChatroomFound;
    public ArrayList chatrooms;
    public Cursor cursor;

    @Override
    public Dialog onCreateDialog(Bundle savedInstance){

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.create_chatroom_layout, null);
        View vh = inflater.inflate(R.layout.chatroom_layout, null);

        chatroom_name = (TextView) view.findViewById(R.id.chatroom_name);
        chatroom_descr = (TextView) view.findViewById(R.id.chatroom_descr);
        seekBar = (SeekBar) view.findViewById(R.id.locationRadiusSeekBar);
        showLocRadius = (TextView) view.findViewById(R.id.showLocationRadius);

        mRecyclerView = (RecyclerView) vh.findViewById(R.id.my_recycler_view);
        noChatroomFound = (LinearLayout) vh.findViewById(R.id.noChatroomFound);

        showLocRadius.setText(seekBar.getProgress() + " km");

        //Change seekbar value
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                showLocRadius.setText(progressValue + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

        });


        builder.setTitle("Create chatroom").setView(view)
               .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {

                       db = new ChatroomDatabase(getActivity());
                       String title = chatroom_name.getText().toString();
                       String description = chatroom_descr.getText().toString();
                       int radius = seekBar.getProgress();

                       if(chatroom_name.getText().length() == 0){
                           chatroom_name.setError("Chatroom title required");
                       }

                       if(chatroom_descr.getText().length() == 0){
                           chatroom_descr.setError("Chatroom title required");
                       }

                       if(chatroom_name.getText().length() > 0 && chatroom_descr.getText().length() > 0) {

                           Long insert = db.InsertNewData(title, description, radius);

                           if (insert != -1) {
                               Toast.makeText(getActivity(), "Chatroom " + title + " created", Toast.LENGTH_LONG).show();

                               displayChatroom(); //Refresh chatroom RecyclerView

                           } else if(insert == -1){
                               Toast.makeText(getActivity(), "Error creating chatroom", Toast.LENGTH_LONG).show();
                           }

                       }

                   }
               })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }

                });

        return  builder.create();

    }

    //display chatroom
    private void displayChatroom() {

        db = new ChatroomDatabase(getActivity());

        chatrooms = getAllChatrooms(); //Get chatrooms
        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(chatrooms.toString()).getAsJsonArray();

        ArrayList<ChatroomResponse> valueData = new ArrayList<ChatroomResponse>();

        for (JsonElement obj : jArray) {
            ChatroomResponse chatroomList = Constants.GSON.fromJson(obj, ChatroomResponse.class);
            valueData.add(chatroomList);
        }

        mRecyclerView.setHasFixedSize(true);

        // Linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        FragmentManager fragmentManager = getFragmentManager();

        //Call adapter
        mAdapter = new ChatroomAdapter(valueData, getActivity(), fragmentManager, noChatroomFound);
        mRecyclerView.setAdapter(mAdapter);

    }



    //Get chatrooms from database and display
    public ArrayList getAllChatrooms(){

        cursor = db.getAllData();
        ArrayList jsonArr = new ArrayList();

        if (cursor.moveToFirst()) {
            do {

                String id = cursor.getString(cursor.getColumnIndex("id"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String description = cursor.getString(cursor.getColumnIndex("description"));
                int radius = cursor.getInt(cursor.getColumnIndex("radius"));

                String jsonData = "{'id' : '" + id + "','title': '" + title + "', 'description': '" + description + "', 'radius': '" + radius + "' }";

                jsonArr.add(jsonData);

            } while (cursor.moveToNext());
        }

        return  jsonArr;

    }


}
